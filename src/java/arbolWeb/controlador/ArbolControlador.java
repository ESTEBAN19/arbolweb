/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolWeb.controlador;

import arbolWeb.controlador.util.JsfUtil;
import arbolse.ArbolSE;
import arbolse.modelo.Arbol;
import arbolse.modelo.Nodo;
import arbolse.modelo.exepciones.ArbolExceptions;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author Edwin Esteban
 */
@Named(value = "arbolControlador")
@SessionScoped
public class ArbolControlador implements Serializable {

    private DefaultDiagramModel model;
    private Arbol arbol = new Arbol();
    private Arbol arbol2 = new Arbol();
    private int dato;
    private String split;
    private String terminado;
    private boolean verInOrden = false;
    private boolean verPosOrden = false;
    private boolean verPreOrden = false;
    private boolean verCantNodos = false;
    private boolean verCantNodosHoja=false;
    private boolean verRetornarAltura=false;
    private boolean verBalanceArb=false;
    private boolean verNumMayor=false;
    private boolean verNumMenor=false;
    private boolean verBorrarNumMenor=false;
    private boolean verBorrarNumMayor=false;
    private boolean verCambiarValores=false;
    private String completoIncompleto="El arbol está compelto";
    private int suma;

    public int getSuma() {
        return suma;
    }

    public void setSuma(int suma) {
        this.suma = suma;
    }
   

    public String getCompletoIncompleto() {
        return completoIncompleto;
    }

    public void setCompletoIncompleto(String completoIncompleto) {
        this.completoIncompleto = completoIncompleto;
    }

    public String getTerminado() {
        return terminado;
    }

    public void setTerminado(String terminado) {
        this.terminado = terminado;
    }

    public String getSplit() {
        return split;
    }

    public void setSplit(String split) {
        this.split = split;
    }

    public boolean isVerCambiarValores() {
        return verCambiarValores;
    }

    public void setVerCambiarValores(boolean verCambiarValores) {
        this.verCambiarValores = verCambiarValores;
    }
    

    public boolean isVerNumMayor() {
        return verNumMayor;
    }

    public void setVerNumMayor(boolean verNumMayor) {
        this.verNumMayor = verNumMayor;
    }

    public boolean isVerNumMenor() {
        return verNumMenor;
    }

    public void setVerNumMenor(boolean verNumMenor) {
        this.verNumMenor = verNumMenor;
    }

    public boolean isVerBorrarNumMenor() {
        return verBorrarNumMenor;
    }

    public void setVerBorrarNumMenor(boolean verBorrarNumMenor) {
        this.verBorrarNumMenor = verBorrarNumMenor;
    }

    public boolean isVerBorrarNumMayor() {
        return verBorrarNumMayor;
    }

    public void setVerBorrarNumMayor(boolean verBorrarNumMayor) {
        this.verBorrarNumMayor = verBorrarNumMayor;
    }

    public boolean isVerCantNodosHoja() {
        return verCantNodosHoja;
    }

    public void setVerCantNodosHoja(boolean verCantNodosHoja) {
        this.verCantNodosHoja = verCantNodosHoja;
    }

    public boolean isVerRetornarAltura() {
        return verRetornarAltura;
    }

    public void setVerRetornarAltura(boolean verRetornarAltura) {
        this.verRetornarAltura = verRetornarAltura;
    }

    public boolean isVerBalanceArb() {
        return verBalanceArb;
    }

    public void setVerBalanceArb(boolean verBalanceArb) {
        this.verBalanceArb = verBalanceArb;
    }

    public boolean isVerCantNodos() {
        return verCantNodos;
    }

    public void setVerCantNodos(boolean verCantNodos) {
        this.verCantNodos = verCantNodos;
    }

    public boolean isVerPosOrden() {
        return verPosOrden;
    }

    public void setVerPosOrden(boolean verPosOrden) {
        this.verPosOrden = verPosOrden;
    }

    public boolean isVerPreOrden() {
        return verPreOrden;
    }

    public void setVerPreOrden(boolean verPreOrden) {
        this.verPreOrden = verPreOrden;
    }

    public boolean isVerInOrden() {
        return verInOrden;
    }

    public void setVerInOrden(boolean verInOrden) {
        this.verInOrden = verInOrden;
    }

    public int getDato() {
        return dato;
    }

    public void setDato(int dato) {
        this.dato = dato;
    }

    public Arbol getArbol() {
        return arbol;
    }

    public void setArbol(Arbol arbol) {
        this.arbol = arbol;
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public void pintarArbol(){
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbol.getRaiz(), model, null, 30, 0);
    }
    
    private void pintarArbol(Nodo reco,DefaultDiagramModel model, Element padre, int x, int y) {
               
        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());
            
            elementHijo.setX(String.valueOf(x)+"em");
            elementHijo.setY(String.valueOf(y)+"em");
            
            if(padre!=null)
            {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre=new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);                
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));        
                
            }    
            
            model.addElement(elementHijo);
            
            pintarArbol(reco.getIzquierda(),model, elementHijo,x-5,y+5);
            pintarArbol(reco.getDerecha(),model,elementHijo,x+5, y+5);
        }
    }

    /**
     * Creates a new instance of ArbolControlador
     */
    public ArbolControlador() {
    }
    public void adicionarNodo() {
        try {
            arbol.adicionarNodo(dato, arbol.getRaiz());
            JsfUtil.addSuccessMessage("El dato ha sido adicionado");
            dato = 0;
            pintarArbol();

        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public void habilitarInOrden() {
        try {
            arbol.isLleno();
            verInOrden = true;
            verPosOrden = false;
            verPreOrden = false;
            verCantNodos = false;
            verCantNodosHoja=false;
            verRetornarAltura=false;
            verBalanceArb=false;
            verNumMayor=false;
            verNumMenor=false;
            verBorrarNumMenor=false;
            verBorrarNumMayor=false;
        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public void habilitarPosOrden() {
        try {
            arbol.isLleno();
            verPosOrden = true;
            verInOrden = false;
            verPreOrden = false;
            verCantNodos = false;
            verCantNodosHoja=false;
            verRetornarAltura=false;
            verBalanceArb=false;
            verNumMayor=false;
            verNumMenor=false;
            verBorrarNumMenor=false;
            verBorrarNumMayor=false;
        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public void habilitarPreOrden() {
        try {
            arbol.isLleno();
            verPreOrden = true;
            verInOrden = false;
            verPosOrden = false;
            verCantNodos = false;
            verCantNodosHoja=false;
            verRetornarAltura=false;
            verBalanceArb=false;
            verNumMayor=false;
            verNumMenor=false;
            verBorrarNumMenor=false;
            verBorrarNumMayor=false;
        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public void habilitarCantNodos()
   {
       try {
            arbol.isLleno();
            verCantNodos=true;
            verInOrden = false;
            verPosOrden = false;
            verPreOrden = false;
            verCantNodosHoja=false;
            verRetornarAltura=false;
            verBalanceArb=false;
            verNumMayor=false;
            verNumMenor=false;
            verBorrarNumMenor=false;
            verBorrarNumMayor=false;
        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }  
   }
     
    public void habilitarCantNodosHoja()
   {
       try {
            arbol.isLleno();
            verCantNodosHoja=true;
            verInOrden = false;
            verPosOrden = false;
            verPreOrden = false;
            verCantNodos = false;
            verRetornarAltura=false;
            verBalanceArb=false;
            verNumMayor=false;
            verNumMenor=false;
            verBorrarNumMenor=false;
            verBorrarNumMayor=false;
        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }  
   }
    
    public void habilitarAlturaArbol()
    {
         try {
            arbol.isLleno();
            verRetornarAltura=true;
            verInOrden = false;
            verPosOrden = false;
            verPreOrden = false;
            verCantNodos = false;
            verCantNodosHoja=false;
            verBalanceArb=false;
            verNumMayor=false;
            verNumMenor=false;
            verBorrarNumMenor=false;
            verBorrarNumMayor=false;
        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }  
    }
           
    public void habilitarBalanceArb()
    {
         try {
            arbol.isLleno();
            verBalanceArb=true;
            verInOrden = false;
            verPosOrden = false;
            verPreOrden = false;
            verCantNodos = false;
            verCantNodosHoja=false;
            verRetornarAltura=false;
            verNumMayor=false;
            verNumMenor=false;
            verBorrarNumMenor=false;
            verBorrarNumMayor=false;
        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }  
    }
    
            
    public void habilitarNumMayor()
    {
         try {
            arbol.isLleno();
            verNumMayor=true;
            verInOrden = false;
            verPosOrden = false;
            verPreOrden = false;
            verCantNodos = false;
            verCantNodosHoja=false;
            verRetornarAltura=false;
            verBalanceArb=false;
            verNumMenor=false;
            verBorrarNumMenor=false;
            verBorrarNumMayor=false;
        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }  
    }
    
    public void habilitarNumMenor()
    {
         try {
            arbol.isLleno();
            verNumMenor=true;
            verInOrden = false;
            verPosOrden = false;
            verPreOrden = false;
            verCantNodos = false;
            verCantNodosHoja=false;
            verRetornarAltura=false;
            verBalanceArb=false;
            verNumMayor=false;
            verBorrarNumMenor=false;
            verBorrarNumMayor=false;
            pintarArbol();
        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }  
    }
        public void habilitarBorrarMenor()
    {
         try {
            arbol.isLleno();
            verBorrarNumMayor=true;
            verInOrden = false;
            verPosOrden = false;
            verPreOrden = false;
            verCantNodos = false;
            verCantNodosHoja=false;
            verRetornarAltura=false;
            verBalanceArb=false;
            verNumMayor=false;
            verNumMenor=false;
            verBorrarNumMenor=false;
            arbol.borrarNumMenor();
            JsfUtil.addSuccessMessage("El dato mayor ha sido borrado");
            pintarArbol();
        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }  
    }
    
    public void habilitarBorrarMayor()
    {
         try {
            arbol.isLleno();
            verBorrarNumMayor=true;
            verInOrden = false;
            verPosOrden = false;
            verPreOrden = false;
            verCantNodos = false;
            verCantNodosHoja=false;
            verRetornarAltura=false;
            verBalanceArb=false;
            verNumMayor=false;
            verNumMenor=false;
            verBorrarNumMenor=false;
            arbol.borrarNumMayor();
            JsfUtil.addSuccessMessage("El dato mayor ha sido borrado");
            pintarArbol();
        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }  
    }
    
    public void habilitarCambiarValores()
    {
         try {
            arbol.isLleno();
            verBorrarNumMenor=true;
            verInOrden = false;
            verPosOrden = false;
            verPreOrden = false;
            verCantNodos = false;
            verCantNodosHoja=false;
            verRetornarAltura=false;
            verBalanceArb=false;
            verNumMayor=false;
            verNumMenor=false;
            verBorrarNumMayor=false;
            arbol.cambiar();
            JsfUtil.addSuccessMessage("Los valores de los nodos han sido cambiados.");
            pintarArbol();
        } catch (ArbolExceptions ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }  
    }
    
    public void extraer(){
        try {
            arbol.split(split);
        } catch (ArbolExceptions ex) {
            Logger.getLogger(ArbolControlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        pintarArbol();
    }
    
    public void terminadosEn(){
        arbol2.terminadosEn(terminado);
        pintarArbol();
    }
    
    public void completo()
    {
        completoIncompleto=arbol.completo();
        JsfUtil.addSuccessMessage(completoIncompleto);
    }
    
    public void suma() throws ArbolExceptions
    {
        try {
        suma=arbol.suma();
        JsfUtil.addSuccessMessage("La suma total del arbol es: "+suma);
        } catch (ArbolExceptions ex) {
            Logger.getLogger(ArbolControlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}



